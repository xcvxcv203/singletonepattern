using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SingletonPattern
{
    public class SingletonUnity : MonoBehaviour
    {
        private static SingletonUnity instance = null;

        public static SingletonUnity Instance
        {
            get
            {
                if (instance == null)
                {
                    SingletonUnity[] allsingletonslnScene = GameObject.FindObjectsOfType<SingletonUnity>();

                    if (allsingletonslnScene != null && allsingletonslnScene.Length > 0)
                    {

                        if (allsingletonslnScene.Length > 1)
                        {
                            Debug.LogWarning("Already Define SingleTone in the scene!");

                            for (int i = 1; i < allsingletonslnScene.Length; i++)
                            {
                                Destroy(allsingletonslnScene[i].gameObject);
                            }
                        }

                        instance = allsingletonslnScene[0];

                        instance.FakeConstructor(); //�ʱ�ȭ
                    }
                    else
                    {
                        Debug.LogError($"Not Define SingleTone Object");
                    }
                }

                return instance;
            }
        }

        private float randomNumber;

        private void FakeConstructor()
        {
            randomNumber = Random.Range(0f, 1f);
        }

        public void TestSingleton()
        {
            Debug.Log($"Hello Singleton, random number is : { randomNumber}");
        }
    }
}
