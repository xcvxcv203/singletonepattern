using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SingletonPattern
{
    public class GameController : MonoBehaviour
    {
        //git �׽�Ʈ
        public void TestCharpSingleton()
        {
            Debug.Log("C#");

            SingletonCSharp instance = SingletonCSharp.Instance;
            instance.TestSingleton();

            SingletonCSharp instance2 = SingletonCSharp.Instance;
            instance2.TestSingleton();
        }

        public void TestUnitySingleton()
        {
            Debug.Log("Unity");

            SingletonUnity instance = SingletonUnity.Instance;
            instance.TestSingleton();

            SingletonUnity instance2 = SingletonUnity.Instance;
            instance2.TestSingleton();
        }
    }
}
